package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Device;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class UserDTO extends RepresentationModel<UserDTO> {

    private UUID id;
    private String username;
    private List<Device> children;

    public List<Device> getChildren() {
        return children;
    }

    public UserDTO(UUID id, String username, List<Device> children) {
        this.id = id;
        this.username = username;
        this.children = children;
    }

    public void setChildren(List<Device> children) {
        this.children = children;
    }

    public UserDTO(UUID id, String username) {
        this.id = id;
        this.username = username;
    }

    public UserDTO() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return username;
    }

    public void setName(String name) {
        this.username = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserDTO userDTO = (UserDTO) o;
        return id.equals(userDTO.id) && username.equals(userDTO.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, username);
    }
}
