package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.UserDetailsDTO;
import ro.tuc.ds2020.entities.UserID;

public class UserBuilder {

    private UserBuilder() {
    }

    public static UserDTO toUserDTO(UserID user) {
        return new UserDTO(user.getId(),user.getName());
    }

    public static UserDetailsDTO toUserDetailsDTO(UserID user) {
        return new UserDetailsDTO(user.getId(), user.getName());
    }

    public static UserID toEntity(UserDetailsDTO userDetailsDTO) {
        return new UserID(userDetailsDTO.getUsername());
    }
}