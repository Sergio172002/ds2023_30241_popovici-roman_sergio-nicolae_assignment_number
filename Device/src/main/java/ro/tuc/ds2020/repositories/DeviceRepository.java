package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Device;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DeviceRepository extends JpaRepository<Device, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Device> findByDescription(String description);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT p " +
            "FROM Device p " +
            "WHERE p.description = :description " +
            "AND p.hourConsumption >= 60  ")
    Optional<Device> findSeniorsByName(@Param("description") String description);

    @Query("SELECT u.username, d.description FROM Device d LEFT JOIN d.user u WHERE u IS NOT NULL")
    List<Object[]> findUserDevicePairs();

    @Query("SELECT d FROM Device d WHERE d.user.id = :userId")
    List<Device> findDevicesByUserId(@Param("userId") UUID userId);

    @Query("SELECT d FROM Device d LEFT JOIN FETCH d.user u WHERE u IS NULL")
    List<Device> findDevicesWithNullUserId();;

    @Modifying
    @Query("UPDATE Device p SET p.hourConsumption = :newHourConsumption WHERE p.id = :uuid")
    int updateNameByUuid(@Param("uuid") UUID uuid, @Param("newHourConsumption") Integer newHourConsumption);
}
