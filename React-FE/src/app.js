import React from 'react'
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom'
import Home from './home/home';
import PersonContainer from './person/person-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import LoginContainer from "./login/login-container";
import {getCurrentUser, logout} from "./login/api/login-api";
import EventBus from "./commons/EventBus";
import {NavLink} from "reactstrap";
import DeviceContainer from "./devices/device-container";
import AssociationsContainer from "./associations/associations-container";
import UserContainer from "./user/user-container";

class App extends React.Component {

    constructor(props) {
        super(props);
        this.logOut = this.logOut.bind(this);

        this.state = {
            showAdminBoard: false,
            currentUser: undefined
        };
    }

    componentDidMount() {
        const user = getCurrentUser();

        if(user){
            this.setState({
                showAdminBoard: user.personDetails.role.includes("ADMIN"),
                currentUser: user
            });
        }

        EventBus.on("logout", () => {
            this.logOut();
        });
    }

    componentWillUnmount() {
        EventBus.remove("logout");
    }

    logOut() {
        localStorage.removeItem("user");
        this.setState({
            showAdminBoard: false,
            currentUser: undefined
        });
    }


    render() {

        const { currentUser, showAdminBoard } = this.state;

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <nav className="navbar navbar-expand navbar-dark bg-dark">
                        <Link to={"/"} className="navbar-brand">
                            Home
                        </Link>
                        <div className="navbar-nav mr-auto">
                            {currentUser ? (
                                <div>
                                    {currentUser && (
                                        <li className="nav-item">
                                            <a href="/login" className="nav-link" onClick={this.logOut}>
                                                LogOut
                                            </a>
                                        </li>
                                    )}
                                </div>
                            ) : (<div className="navbar-nav ml-auto">
                                    <li className="nav-item">
                                        <NavLink href="/login">LogIn</NavLink>
                                    </li>
                                </div>
                            )
                            }
                            {console.log(showAdminBoard+"")}
                            {showAdminBoard && (
                                <li className="nav-item">
                                    <Link to={"/person"} className="nav-link">
                                        Person Tab
                                    </Link>
                                </li>
                            )}

                            {showAdminBoard && (
                                <li className="nav-item">
                                    <Link to={"/device"} className="nav-link">
                                        Devices Tab
                                    </Link>
                                </li>
                            )}

                            {showAdminBoard && (
                                <li className="nav-item">
                                    <Link to={"/associations"} className="nav-link">
                                        Associations Tab
                                    </Link>
                                </li>
                            )}

                            {currentUser && !showAdminBoard && (
                                <li className="nav-item">
                                    <Link to={"/user"} className="nav-link">
                                        Devices Tab
                                    </Link>
                                </li>
                            )}
                        </div>

                    </nav>

                    <Switch>
                        <Route exact path='/' render={() => <Home/>}/>

                        {currentUser !== undefined && showAdminBoard && (
                            <Route exact path='/person' render={() => <PersonContainer />} />
                        )}

                        <Route exact path='/user' render={() => <UserContainer />} />

                        {currentUser !== undefined && showAdminBoard && (
                            <Route exact path='/associations' render={() => <AssociationsContainer />} />
                        )}
                        {currentUser !== undefined && showAdminBoard && (
                            <Route exact path='/device' render={() => <DeviceContainer />} />
                        )}
                        <Route exact path='/login' render={() => <LoginContainer/>}/>
                        {/*Error*/}
                        <Route exact path='/error' render={() => <ErrorPage/>}/>
                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
