import {HOST_DEVICES, HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";



const endpoint = {
    device: '/device',
    user: '/user',
    free: '/free',
    create: '/create',
    pairs:'/pairs',
    per_user: '/per_user'
};

function getUsers(callback) {
    let request = new Request(HOST_DEVICES.backend_api + endpoint.user, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getDevicesOfLoggedUser(id, callback){
    let request = new Request(HOST_DEVICES.backend_api + endpoint.device + endpoint .per_user+ "/"+ id , {
        method: 'GET',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export{
    getUsers,
    getDevicesOfLoggedUser
};