import React from "react";
import Table from "../../commons/tables/table";


const columns = [
    {
        Header: 'Description',
        accessor: 'description',
    },
    {
        Header: 'HourConsumption',
        accessor: 'hourConsumption',
    },
];

const filters = [
    {
        accessor: 'name',
    }
];

class UserTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={4}
            />
        )
    }
}

export default UserTable;