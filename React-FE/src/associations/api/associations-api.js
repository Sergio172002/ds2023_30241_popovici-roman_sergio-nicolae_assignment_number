import {HOST_DEVICES, HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";



const endpoint = {
    device: '/device',
    user: '/user',
    free: '/free',
    create: '/create',
    pairs:'/pairs'
};

function getUsers(callback) {
    let request = new Request(HOST_DEVICES.backend_api + endpoint.user, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getDevicesWithNoUser(callback) {
    let request = new Request(HOST_DEVICES.backend_api + endpoint.device + endpoint.free, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getDevicesUsersPairs(callback) {
    let request = new Request(HOST_DEVICES.backend_api + endpoint.device + endpoint.pairs, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postAssociation(value, callback){
    let request = new Request(HOST_DEVICES.backend_api + endpoint.user + endpoint.create , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(value)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export{
  getUsers,
  getDevicesWithNoUser,
  postAssociation,
  getDevicesUsersPairs
};