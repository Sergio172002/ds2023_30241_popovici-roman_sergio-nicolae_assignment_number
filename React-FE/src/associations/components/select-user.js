import React, {Component} from 'react';
import Select from 'react-select';
import * as API_USERS from '../../associations/api/associations-api.js';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message"; // Import your API functions

class SelectUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userOptions: [],
            selectedUser: null,
            isLoaded: false,
            errorStatus: null,
            error: null,
        };
    }

    processResult = (result) => {
        // Process the result data here as needed
        return result.map(item => ({
            value: item.id,
            label: item.name,
        }));
    };

    fetchUsers() {
        return API_USERS.getUsers((result, status, err) => {
            if (result !== null && (status === 200 || status === 202)) {
                const processedData = this.processResult(result);
                console.log(processedData)
                this.setState({
                    userOptions: processedData,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    componentDidMount() {
        // Fetch device data and populate the options
        this.fetchUsers();
    }

    handleUserSelect = selectedUser => {
        this.setState({ selectedUser });
        this.props.onUserSelect(selectedUser);
    };

    render() {
        return (
            <div>
                <Select
                    value={this.state.selectedUser}
                    onChange={this.handleUserSelect}
                    options={this.state.userOptions}
                    isSearchable={true}
                    placeholder="Select an user"
                />
                {this.state.errorStatus && (
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} />
                )}
            </div>
        );
    }
}

export default SelectUser;
