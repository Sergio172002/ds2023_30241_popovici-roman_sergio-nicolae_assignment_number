import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader,
    Col,
    Row
} from 'reactstrap';
import SelectUser from "./components/select-user";
import SelectDevice from "./components/select-device";
import * as API_USERS from "../devices/api/device-api";
import * as API_ASSOCIATION from "../associations/api/associations-api";
import Button from "react-bootstrap/Button";
import DeviceTable from "../devices/components/device-table";
import PairsTable from "./components/pairs-table";

class AssociationsContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedDevice: null,
            selectedUser: null,
            selected: false,
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }


    handleUserSelect = (selectedUser) => {
        this.setState({ selectedUser });
        console.log(selectedUser);
    };

    handleDeviceSelect = (selectedDevice) => {
        this.setState({ selectedDevice });
        console.log(selectedDevice);
    };

    fetchAssociation() {
        const { selectedUser, selectedDevice } = this.state;
        const requestBody = {
            parentId: selectedUser.value,
            childId: selectedDevice.value
        };
        console.log(requestBody);
        return API_ASSOCIATION.postAssociation(requestBody, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201 || status === 202)) {
                console.log("Successfully updated device with id: " + result + status + error);
                // this.reloadHandler();
            } else {
                this.setState({
                    errorStatus: status,
                    error: error
                });
            }
        });
    }

    fetchPairs() {
        return API_ASSOCIATION.getDevicesUsersPairs((result, status, err) => {
            if (result !== null && (status === 200 || status === 202 )) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }
    componentDidMount() {
        this.fetchPairs();
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    handleSubmit = () => {
        this.fetchAssociation();
        this.reload();
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPairs();
    }

    render() {
        return (
            <div style={{ marginTop: '10px'} }>
                <CardHeader>
                    <strong>Associations Management</strong>
                </CardHeader>
                <Card style={{ marginTop: '30px'} } >
                    <Row>
                        <Col sm="6">
                            <SelectUser onUserSelect={this.handleUserSelect} />
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                        <Col sm="6">
                            <SelectDevice onDeviceSelect={this.handleDeviceSelect} />
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: '30px'} }>
                        <Col sm={{ size: '4', offset: 4 }}>
                            <div className="d-flex justify-content-center">
                                <Button type="submit" onClick={this.handleSubmit}>Submit</Button>
                            </div>
                        </Col>
                    </Row>
                    <Row  style={{ marginTop: '100px'}}>
                        <Col sm={{size: '8', offset: 2}}>
                            {this.state.isLoaded && <PairsTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>
                <Card>

                </Card>
            </div>
        );
    }
}

export default AssociationsContainer;
