import {HOST_DEVICES} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";



const endpoint = {
    person: '/device'
};

function getDevices(callback) {
    let request = new Request(HOST_DEVICES.backend_api + endpoint.person, {
        method: 'GET',
    });
    console.log(request.url);

    RestApiClient.performRequest(request, callback);
}

function getDeviceById(params, callback){
    let request = new Request(HOST_DEVICES.backend_api + endpoint.person + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postDevice(user, callback){
    let request = new Request(HOST_DEVICES.backend_api + endpoint.person , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);
    console.log(user);

    RestApiClient.performRequest(request, callback);
}

function putDevice(updatedValue, id, callback){
    let request = new Request(HOST_DEVICES.backend_api + endpoint.person + "/"+ id , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedValue)
    });

    console.log(JSON.stringify(updatedValue))
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteDevice(id, callback){
    let request = new Request(HOST_DEVICES.backend_api + endpoint.person + "/"+ id , {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    deleteDevice,
    getDevices,
    getDeviceById,
    postDevice,
    putDevice
};
