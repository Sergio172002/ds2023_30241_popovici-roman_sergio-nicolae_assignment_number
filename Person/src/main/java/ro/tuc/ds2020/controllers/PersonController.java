package ro.tuc.ds2020.controllers;

import io.jsonwebtoken.Jwts;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.services.PersonService;

import javax.security.sasl.AuthenticationException;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping()
    public ResponseEntity<List<PersonDTO>> getPersons() {
        List<PersonDTO> dtos = personService.findPersons();
        for (PersonDTO dto : dtos) {
            Link personLink = linkTo(methodOn(PersonController.class)
                    .getPerson(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody PersonDetailsDTO personDTO) {
        UUID personID = personService.insert(personDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteUser(@PathVariable("id") UUID personId){
        UUID message = personService.deleteById(personId);
        return new ResponseEntity<>(message, HttpStatus.ACCEPTED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UUID> updateAtUuid(@PathVariable("id") UUID personId, @Valid @RequestBody UpdatePersonDTO updateValueDTO){
        int newValue = updateValueDTO.getUpdatedValue();
        System.out.println(newValue+" "+personId);
        UUID message = personService.updateAtId(personId,newValue);
        return new ResponseEntity<>(message,HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PersonDetailsDTO> getPerson(@PathVariable("id") UUID personId) {
        PersonDetailsDTO dto = personService.findPersonById(personId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping(value = "/login")  // Assuming you want to handle login at this endpoint
    public ResponseEntity<LoginResponse> logInUser(@Valid @RequestBody LoginRequest loginRequest) throws AuthenticationException {
        String username = loginRequest.getUsername();
        String password = loginRequest.getPassword();
        System.out.println("Sunt aici");

        // Call the login method from your service to handle authentication
        PersonDetailsDTO personDetails = personService.login(username, password);

        // Generate a JWT token
        String token = generateJwtToken(username);

        // Create a response object containing user details and the token
        LoginResponse loginResponse = new LoginResponse(personDetails, token);

        return new ResponseEntity<>(loginResponse, HttpStatus.OK);
    }

    private String generateJwtToken(String username) {
        // You can customize the token generation as per your requirements
        String secretKey = "yourSecretKey"; // Replace with a strong secret key
        long expirationMillis = 86400000; // Example: 1 day

        String token = Jwts.builder()
                .claim("name", "Jane Doe")
                .setSubject(username)
                .setId(UUID.randomUUID().toString())
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plus(5l, ChronoUnit.MINUTES)))
                .compact();

        return token;
    }

    //TODO: UPDATE per resource

}
