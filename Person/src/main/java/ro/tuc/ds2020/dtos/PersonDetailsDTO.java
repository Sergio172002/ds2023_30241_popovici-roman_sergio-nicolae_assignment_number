package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.dtos.validators.annotation.AgeLimit;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class PersonDetailsDTO {

    private UUID id;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @AgeLimit(limit = 18)
    private int age;
    @NotNull
    private String role;

    public PersonDetailsDTO() {
    }

    public PersonDetailsDTO( String username, String password, int age, String role) {
        this.username = username;
        this.password = password;
        this.age = age;
        this.role = role;
    }

    public PersonDetailsDTO(UUID id, String username, String password, int age, String role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.age = age;
        this.role = role;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setRole(String role){ this.role = role; }

    public String getRole(){ return role; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonDetailsDTO that = (PersonDetailsDTO) o;
        return age == that.age &&
                Objects.equals(username, that.username) &&
                Objects.equals(password, that.password) &&
                Objects.equals(role, that.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, age, role);
    }
}
