package ro.tuc.ds2020.dtos;

public class LoginResponse {
    private PersonDetailsDTO personDetails;
    private String token;

    public LoginResponse() {
    }

    public LoginResponse(PersonDetailsDTO personDetails, String token) {
        this.personDetails = personDetails;
        this.token = token;
    }

    public PersonDetailsDTO getPersonDetails() {
        return personDetails;
    }

    public void setPersonDetails(PersonDetailsDTO personDetails) {
        this.personDetails = personDetails;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
