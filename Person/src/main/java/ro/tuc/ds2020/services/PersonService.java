package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.PersonRepository;
import org.springframework.web.client.RestTemplate;

import javax.security.sasl.AuthenticationException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final PersonRepository personRepository;
    private final RestTemplate restTemplate;

    @Autowired
    public PersonService(PersonRepository personRepository, RestTemplate restTemplate) {
        this.personRepository = personRepository;
        this.restTemplate = restTemplate;
    }

    public List<PersonDTO> findPersons() {
        List<Person> personList = personRepository.findAll();
        return personList.stream()
                .map(PersonBuilder::toPersonDTO)
                .collect(Collectors.toList());
    }

    public PersonDetailsDTO findPersonById(UUID id) {
        Optional<Person> prosumerOptional = personRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return PersonBuilder.toPersonDetailsDTO(prosumerOptional.get());
    }

//    public UUID insert(PersonDetailsDTO personDTO) {
//        Person person = PersonBuilder.toEntity(personDTO);
//        person = personRepository.save(person);
//        LOGGER.debug("Person with id {} was inserted in db", person.getId());
//        return person.getId();
//    }

    public UUID insert(PersonDetailsDTO personDTO) {

        UUID personUUID = UUID.randomUUID();

        Person person = PersonBuilder.toEntity(personDTO);
        person.setId(personUUID);
        person = personRepository.save(person);
        LOGGER.debug("Person with id {} was inserted in db", person.getId());
        if(person.getRole().equals("USER")) {
            String targetUrl = "http://172.16.238.6:8081/user";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            personDTO.setId(personUUID);
            // Prepare the request entity with the necessary headers and body
            HttpEntity<PersonDetailsDTO> requestEntity = new HttpEntity<>(personDTO, headers);

            ResponseEntity<UUID> responseEntity = restTemplate.exchange(targetUrl, HttpMethod.POST, requestEntity, UUID.class);

            // Handle the response as needed
            UUID targetAppResponse = responseEntity.getBody();
        }

        return person.getId();
    }

    public UUID deleteById(UUID id) {
        Optional<Person> prosumerOptional = personRepository.findById(id);

        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }else{
            LOGGER.error("Person with id {} was deleted from the db", id);
            personRepository.deleteById(id);

            Person person = prosumerOptional.get();
            if(person.getRole().equals("USER")){
                String targetUrl = "http://172.16.238.6:8081/user/"+id;
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);

                // Prepare the request entity with the necessary headers and body
                HttpEntity<?> requestEntity = new HttpEntity<>(headers);

                ResponseEntity<UUID> responseEntity = restTemplate.exchange(targetUrl, HttpMethod.DELETE, requestEntity, UUID.class);

                // Handle the response as needed
                // targetAppResponse = responseEntity.getBody();
            }
        }
        return id;
    }

    @Transactional
    public UUID updateAtId(UUID id, int age) {
        Optional<Person> prosumerOptional = personRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }else{
            LOGGER.error("Person with id {} was updated", id);
            personRepository.updateNameByUuid(id, age);
        }

        // Create a simple JSON object with a key-value pair
        return id;
    }

    //Complete Functions:.....
    public PersonDetailsDTO login(String username, String password) throws AuthenticationException {
        Optional<Person> personOptional = personRepository.findByUsername(username);

        if (!personOptional.isPresent()) {
            // Handle the case where the username is not found in the database
            LOGGER.error("Person with username {} was not found in db", username);
            throw new ResourceNotFoundException("Person with username: " + username);
        }

        Person person = personOptional.get();

        // Check if the provided password matches the stored password in the database
        if (!password.equals(person.getPassword())) {
            // Handle the case where the password is incorrect
            LOGGER.error("Incorrect password for Person with username: {}", username);
            throw new AuthenticationException("Incorrect password for username: " + username);
        }

        // If username and password match, create and return a PersonDetailsDTO
        return PersonBuilder.toPersonDetailsDTO(person);
    }



}
