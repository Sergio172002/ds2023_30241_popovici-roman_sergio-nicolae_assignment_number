package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Person;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PersonRepository extends JpaRepository<Person, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    Optional<Person> findByUsername(String username);

    /**
     * Example: Write Custom Query
     **/
    @Query(value = "SELECT p " +
            "FROM Person p " +
            "WHERE p.username = :username " +
            "AND p.age >= 60  ")
    Optional<Person> findSeniorsByName(@Param("username") String username);


    @Modifying
    @Query("UPDATE Person p SET p.age = :newAge WHERE p.id = :uuid")
    int updateNameByUuid(@Param("uuid") UUID uuid, @Param("newAge") Integer newAge);


}

